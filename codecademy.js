//jut testing git
// CouchDB support
var nano = require('nano')('http://localhost:5984');
var students = nano.use('students');

// WWW support
var express = require("express");
var app = express();
app.use(express.static(__dirname + '/public'));
app.use(express.bodyParser());

// Webscraping support
var jsdom  = require('jsdom');
var fs     = require('fs');
var jquery = fs.readFileSync("./jquery-1.8.2.min.js").toString();

// Get points from internet
var getPoints = function(user,func){
  jsdom.env({
    html: 'http://www.codecademy.com/users/' + user.username,
    src: [jquery],
    done: function(errors, window) {
      if (!errors) {
        var $ = window.$;
        user.points = parseInt($('div.box.total_points_box.point_box h3.point_count').text());
        func(user);       
      }
    }
  });
};

var checkCodecademyForNewScore = function(){
  students.list({include_docs:true},function(e,body){
    if(!e){
      var timeToWait = 0;
      body.rows.forEach(function(row) {
        setTimeout(function(){
          var oldPoints = row.doc.points;
          getPoints(row.doc,function(user){
            if(oldPoints!==user.points){
              students.insert(user, function(e, body){
                if(e){
                  console.log("Error: updating user.");
                }
              });
            }
          });
        },timeToWait);
        timeToWait += 10000; // wait 10 sec
      });   
    } 
  });
};

setInterval(checkCodecademyForNewScore,60000*10); // 60000 = 1/min * 10 = 10 min interval

// get all students from couch and send them to the client.
app.get("/user", function(req,res){
  students.list({include_docs:true},function(e,body){
    var students = [];
    if(!e){
       body.rows.forEach(function(row) {
          students.push(row.doc);    
       });
       students.sort(function(a,b){return b.points - a.points});
       res.json(students, 200);
    } else {
      res.json({}, 500);
    }
  });
});

app.post("/user",function(req,res){
  getPoints(req.body,function(user){
    var hasPoints =!isNaN(user.points);
    var notInList = true;
    students.list({include_docs:true},function(e,body){
      if(!e) {
        body.rows.forEach(function(row) {
          if (row.doc.username === user.username){
            notInList = false;
          }
        });
        if(notInList && hasPoints) {
          students.insert(user, function(e, body){
            if(!e) {
              res.json({ok:true}, 201); 
            } 
          });
        } else {
          res.json({ok:false}, 201);
        }
      }
    });  
  });  
});

app.get("/changes", function(req,res){
  nano.db.changes('students', {since: req.query.seq, feed:"longpoll"}, function(e,b){
    if(b.last_seq !== req.query.seq){
      res.json({change:true, last_seq: b.last_seq}, 200);
    }
  });
});

app.listen(8006);
console.log("static server running on port 8006");